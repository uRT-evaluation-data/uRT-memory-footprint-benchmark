# µRT Memory Footprint Benchmarks

Contents of this directory:

- Benchmark result data of the µRT benchmark application [1,2] for various modules and microcontrollers:
  - [DiWheelDrive v1.1](DiWheelDrive_1-1.tsv): STM32F1 (ARM Cortex-M3)
  - [DiWheelDrive v1.2](DiWheelDrive_1-2.tsv): STM32F1 (ARM Cortex-M3)
  - [LightRing v1.0](LightRing_1-0.tsv): STM32F1 (ARM Cortex-M3)
  - [LightRing v1.2](LightRing_1-2.tsv): STM32F1 (ARM Cortex-M3)
  - [NUCLEO-F103RB](NUCLEO-F103RB.tsv): STM32F1 (ARM Cortex-M3)
  - [NUCLEO-F401RE](NUCLEO-F401RE.tsv): STM32F4 (ARM Cortex-M4)
  - [NUCLEO-F767ZI](NUCLEO-F767ZI.tsv): STM32F7 (ARM Cortex-M7)
  - [NUCLEO-G071RB](NUCLEO-G071RB.tsv): STM32G0 (ARM Cortex-M0+)
  - [NUCLEO-L476RG](NUCLEO-L476RG.tsv): STM32L4 (ARM Cortex-M4)
  - [PowerManagement v1.1](PowerManagement_1-1.tsv): STM32F4 (ARM Cortex-M4)
  - [PowerManagement v1.2](PowerManagement_1-2.tsv): STM32F4 (ARM Cortex-M4)
  - [STM32F407G-DISC1](STM32F407G-DISC1.tsv): STM32F4 (ARM Cortex-M4)
- [`LICENSE`](LICENSE) file:<br>
  Data may be accessed, modified and republished according to the Creative Commons Attribution 4.0 International license (CC-BY).

Each of the `.tsv` (tab separated values) files contains a table with the static memory footprints of various configuration setups of µRT.
The first row 'OS' contains the memory footprint without µRT and can be used as baseline.
Therefore, the overhead of each configuration, compared to a build of the blank operating system only, is stated in the colums labeled 'difference'.
Finally, empty cells imply 'n/a', for instance if the configuration would exceed the available flash memory and linking failed (cf. [NUCLEO-G071RB](NUCLEO-G071RB.tsv)).

---

**[1]** Schöpping T, Kenneweg S. "µRT". Bielefeld University; 2022. DOI: [10.4119/unibi/2966336](https://doi.org/10.4119/unibi/2966336)

**[2]** https://gitlab.ub.uni-bielefeld.de/AMiRo/AMiRo-Apps/-/blob/main/configurations/uRT-Benchmark/evaluation/Build.py
